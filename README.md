## This is a small collection of apps.
They focus on different beginner principles, as I am a beginner. They are from the Devslopes course on iOS 11 and Swift 4 app development on Udemy, which I work on in my little spare time.

Working on these projects and more have awaken a passion for app development. It's what I want to do for a career and
I can't wait to grind out more of these lessons and take more classes on app development over the summer.

So, yeah, its a small collection. But it'll be full this summer.
