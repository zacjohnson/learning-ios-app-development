//
//  ViewController.swift
//  hustle-mode
//
//  Created by Zac Johnson on 10/2/17.
//  Copyright © 2017 Zac Johnson. All rights reserved.
//

// TODO: Make it adjust to screen size for larger/smaller phones

import UIKit
import AVFoundation // framework to play audio

class ViewController: UIViewController {

	@IBOutlet weak var DarkBlueBG: UIImageView!
	@IBOutlet weak var powerButton: UIButton!
	@IBOutlet weak var cloudHolder: UIView!
	@IBOutlet weak var rocket: UIImageView!
	@IBOutlet weak var hustleLbl: UILabel!
	@IBOutlet weak var onLbl: UILabel!
	
	var player: AVAudioPlayer!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		let path = Bundle.main.path(forResource: "hustle-on", ofType: "wav")!
		let url = URL(fileURLWithPath: path)
		do {
			player = try AVAudioPlayer(contentsOf: url)
			player.prepareToPlay()
		} catch let error as NSError {
			print(error.description)
		}
	}
	
	
	@IBAction func powerBtnPressed(_ sender: Any) {
		cloudHolder.isHidden = false
		DarkBlueBG.isHidden = true
		powerButton.isHidden = true
		
		player.play()
		
		UIView.animate(withDuration: 2.3, animations: {
			self.rocket.frame = CGRect(x: 0, y: 140, width: 375, height: 402)
		}) { (finished) in
			self.hustleLbl.isHidden = false
			self.onLbl.isHidden = false
		}
	}
}

